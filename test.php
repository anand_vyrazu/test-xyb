<?php

class LoginLog {
    public static function checkLoginLog($log_data) {
        // get the separate lines
        $separate_lines = explode(PHP_EOL, $log_data);
        $login_data = [];
        $result = '';

        // separate the datetime and customer id
        foreach( $separate_lines as $line ) {
            $value = explode(" ", $line);

            $customer_id = $value[3];
            $date = $value[0];//." ".$value[1]." ".$value[2]; ==> This ignores the time

            $login_data[$customer_id][] = strtotime($date);
        }

        foreach( $login_data as $customer => $logins ) {
            $is_consecutive_login = true;
            $consicutive_days = 1;

            // sort the array
            $logins = self::_vsort($logins);

            if( count($logins) < 3 ) {
                $is_consecutive_login = false;
            }
            else {
                foreach( $logins as $index => $login_date ) {
                    if( $index != 0 ) {
                        $diff = $login_date - $logins[$index -1];
                        
                        if($diff == 0) {
                            $last_login_date = self::_findLastLoginDate($logins, $index);
                            $diff = $login_date - $last_login_date;
                        }
                        
                        if( $diff != 0 ) {
                            $consicutive_days++;
                        }

                        if( $diff > 86400 && $consicutive_days <= 3) {
                            // reset the consicutive counter
                            $consicutive_days = 1;

                            $is_consecutive_login = false;
                        }
                        else {
                            $is_consecutive_login = true;
                        }
                    }       
                }    
            }

            if( $is_consecutive_login && $consicutive_days >= 3 ) {
                $result .= $customer .",";
            }
        }

        return rtrim($result, ",");
    }

    private static function _findLastLoginDate($login_dates, $index) {
        for($i = $index; $i < (count($login_dates)); $i++) {
            if( ($login_dates[$i] - $login_dates[$i - 1]) > 0 ) {
                return $login_dates[$i - 1];
            }
        }
    }

    private static function _vsort($data) {
        asort($data);
        $sorted_array = [];

        foreach( $data as $item ) {
            $sorted_array[] = $item;
        }

        return $sorted_array;
    }
}
